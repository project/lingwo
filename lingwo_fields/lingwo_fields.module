<?php

/**
 * @file
 * All the hooks for the 'lingwo_fields' module.
 */

// get our API functions for dealing with fields
require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'lingwo_fields') . '/' . 'lingwo_fields.api.inc';
// the code for generating the bits on the node form (here for organizational purposes)
require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'lingwo_fields') . '/' . 'lingwo_fields.widget.inc';

/**
 * Implements hook_menu().
 */
function lingwo_fields_menu() {
  $items = array();

  $items['admin/config/lingwo/fields'] = array(
    'title' => 'Fields',
    'description' => 'Settings for Lingwo fields module.',
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lingwo_fields_admin_settings_form'),
    'file' => 'lingwo_fields.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_help().
 */
function lingwo_fields_help($path, $arg) {
  switch ($path) {
    case 'admin/help#lingwo_fields':
      return '<p>' . t('Adds support for "fields" to dictionary entries.  It allows you to specify multiple forms of each word, for example: plural of nouns or conjugation of verbs.  Configure it !here.', array('!here' => l(t('here'), 'admin/config/lingwo/fields'))) . '</p>';
    case 'admin/settings/lingwo/fields':
      return '<p>' . t('Configure the fields provided for each <em>Part of speech</em> on a dictionary entry.') . '</p>';
  }
}

/**
 * Implements hook_permission().
 */
function lingwo_fields_permission() {
  return array(
    'edit Lingwo fields JavaScript' => array(
      'title' => t('Edit dictionary fields JavaScript'),
      'restrict access' => TRUE,
    ),
  );
}

/**
 * Implements hook_lingwo_entry_properties().
 */
function lingwo_fields_lingwo_entry_properties() {
  return array(
    'fields' => array(
      'class' => 'LingwoFields',
    ),
  );
}

/**
 * Implements hook_theme().
 */
function lingwo_fields_theme() {
  return array(
    'lingwo_fields_widget_form' => array(
      'render element' => 'element',
      'pattern' => 'lingwo_fields_widget_form__',
    ),
    'lingwo_fields_formatter_default' => array(
      'variables' => array('node' => NULL),
    ),
    'lingwo_fields_view' => array(
      'render element' => 'element',
      'pattern' => 'lingwo_fields_view__',
    ),
  );
}

/**
 * Implements hook_lingwo_entry_form_alter().
 */
function lingwo_fields_lingwo_entry_form_alter(&$form, &$form_state, $node) {
  // TODO: should we pass $entry in?
  $entry = LingwoEntry::fromNode($node);
  if (!$entry->isTranslation()) {
    // only applies to entries that are not translations
    $form['lingwo_fields'] = _lingwo_fields_widget($form, $form_state, $entry);
  }
}

/**
 * Implements hook_field_extra_fields().
 */
function lingwo_fields_field_extra_fields() {
  $type = LingwoEntry::$settings->content_type;

  foreach (array('form', 'display') as $which) {
    $extra['node'][$type][$which] = array(
      'lingwo_fields' => array(
        'label' => t('Lingwo Fields'),
        'description' => t('Lingwo dictionary "Fields" field.'),
        'weight' => 1,
      ),
    );
  }

  return $extra;
}

/**
 * Implementation of hook_file_download().
 */
// TODO: test if this still works in Drupal 7!
function lingwo_fields_file_download($uri) {
  // This is necessary if private files are enabled.
  if (preg_match(',lingwo/languages/.*?\.js,', $filepath)) {
    return array('Content-type: application/javascript');
  }
}

function _lingwo_fields_clean(&$items) {
  foreach ($items as &$item) {
    unset($item['remove']);

    if ($item['type'] == 'class') {
      $item['value'] = intval($item['value']);
    }
    $item['automatic'] = isset($item['automatic']) ? (bool)$item['automatic'] : FALSE;

    if (isset($item['alt'])) {
      $item['alt'] = array_filter($item['alt']);
      if (count($item['alt']) == 0) {
        unset($item['alt']);
      }
    }
  }
}


function _lingwo_fields_write_record($fields, $is_update) {
  if ($is_update) {
    drupal_write_record('lingwo_fields_revisions', $fields, 'vid');
  }
  else {
    drupal_write_record('lingwo_fields_revisions', $fields);
  }

  # update our lookup table!
  # TODO: in the future we will have many more lookup tables!
  db_delete('lingwo_fields_forms')
    ->condition('nid', $fields->nid)
    ->execute();
  foreach ($fields->data as $field) {
    if ($field['type'] == 'form') {
      $values = array($field['value']);
      if (isset($field['alt'])) {
        $values = array_merge($values, $field['alt']);
      }

      foreach ($values as $value) {
        // don't add an empty value!
        if (is_null($value) || $value == '') {
          continue;
        }

        $form = (object) array(
          'nid' => $fields->nid,
          'form' => $value,
          'form_name' => $field['name'],
        );
        drupal_write_record('lingwo_fields_forms', $form);
      }
    }
  }
}

/**
 * Implements hook_node_view().
 */
function lingwo_fields_node_view($node, $view_mode = 'full') {
  if (LingwoEntry::isEntryNode($node)) {
    $node->content['lingwo_fields'] = array(
      '#type' => 'markup',
      '#markup' => theme('lingwo_fields_formatter_default', array('node' => $node)),
    );
  }
}

/**
 * Implements hook_node_load().
 */
function lingwo_fields_node_load($nodes, $types) {
  if (in_array(LingwoEntry::$settings->content_type, $types)) {
    $res = db_query("SELECT nid, data FROM {lingwo_fields_revisions} WHERE vid IN (:vid)",
      array(':vid' => array_map(function ($node) { return $node->vid; }, $nodes)));

    foreach ($res as $row) {
      $nodes[$row->nid]->lingwo_fields = unserialize($row->data);
    }
  }
}

/**
 * Implements hook_node_presave().
 */
function lingwo_fields_node_presave($node) {
  lingwo_fields_node_prepare($node);
}

/**
 * Implements hook_node_prepare().
 */
function lingwo_fields_node_prepare($node) {
  if (LingwoEntry::isEntryNode($node) && !_lingwo_is_translation($node)) {
    if (!empty($node->lingwo_fields)) {
      _lingwo_fields_clean($node->lingwo_fields);
    }
    elseif (!isset($node->lingwo_fields) || is_null($node->lingwo_fields)) {
      $node->lingwo_fields = array();
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function lingwo_fields_node_insert($node) {
  if (LingwoEntry::isEntryNode($node) && !_lingwo_is_translation($node)) {
    $object = (object) array(
      'nid' => $node->nid,
      'vid' => $node->vid,
      'data' => $node->lingwo_fields,
    );

    _lingwo_fields_write_record($object, FALSE);
  }
}

/**
 * Implements hook_node_update().
 */
function lingwo_fields_node_update($node) {
  if (LingwoEntry::isEntryNode($node) && !_lingwo_is_translation($node)) {
    $object = (object) array(
      'nid' => $node->nid,
      'vid' => $node->vid,
      'data' => $node->lingwo_fields,
    );

    _lingwo_fields_write_record($object, empty($node->revision));
  }
}

/**
 * Implements hook_node_revision_delete().
 */
function lingwo_fields_node_revision_delete($node) {
  if ($node->type == LingwoEntry::$settings->content_type && !_lingwo_is_translation($node)) {
    db_delete('lingwo_fields_revisions')
      ->condition('vid', $node->vid)
      ->execute();
  }
}

/**
 * Implements hook_node_delete().
 */
function lingwo_fields_node_delete($node) {
  if (LingwoEntry::isEntryNode($node) && !_lingwo_is_translation($node)) {
    db_delete('lingwo_fields_revisions')
      ->condition('nid', $node->nid)
      ->execute();

    db_delete('lingwo_fields_forms')
      ->condition('nid', $node->nid)
      ->execute();
  }
}

/**
 * Implements hook_lingwo_language_form().
 */
function lingwo_fields_lingwo_language_form($language) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'lingwo_fields') . '/' . 'lingwo_fields.admin.inc';
  return _lingwo_fields_lingwo_language_form($language);
}

/**
 * Implements hook_lingwo_language_form_validate().
 */
function lingwo_fields_lingwo_language_form_validate($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'lingwo_fields') . '/' . 'lingwo_fields.admin.inc';
  return _lingwo_fields_lingwo_language_form_validate($form, $form_state);
}

/**
 * Implements hook_lingwo_language_form_submit().
 */
function lingwo_fields_lingwo_language_form_submit($form, &$form_state) {
  require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'lingwo_fields') . '/' . 'lingwo_fields.admin.inc';
  return _lingwo_fields_lingwo_language_form_submit($form, $form_state);
}

/**
 * Implements hook_lingwo_language_save().
 */
function lingwo_fields_lingwo_language_save($language) {
  $code = $language->getValue('language', $language->getNode()->nid);
  LingwoFields::generateJavascript($code, $language);
}

