<?php

/**
 * @file
 * All the hooks for the 'lingwo_senses' module.
 */

// get our API functions for dealing with senses
require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'lingwo_senses') . '/' . 'lingwo_senses.api.inc';
// the code for generating the bits on the node form (here for organizational purposes)
require_once DRUPAL_ROOT . '/' . drupal_get_path('module', 'lingwo_senses') . '/' . 'lingwo_senses.widget.inc';

/**
 * Implements hook_menu().
 */
function lingwo_senses_menu() {
  $items = array();

  $items['admin/config/lingwo/senses'] = array(
    'title' => 'Senses',
    'description' => 'Settings for Lingwo Senses module.',
    'access arguments' => array('administer site configuration'),
    'page callback' => 'drupal_get_form',
    'page arguments' => array('lingwo_senses_admin_settings_form'),
    'file' => 'lingwo_senses.admin.inc',
    'type' => MENU_LOCAL_TASK,
  );

  return $items;
}

/**
 * Implements hook_help().
 */
function lingwo_senses_help($path, $arg) {
  switch ($path) {
    case 'admin/help#lingwo_senses':
      return '<p>' . t('Adds support for "multiple senses" to dictionary entries.  It allows users to state how senses are different, provide examples and translate them individually.  Configure it !here.', array('!here' => l(t('here'), 'admin/config/lingwo/senses'))) . '</p>';
    case 'admin/config/lingwo/senses':
      return '<p>' . t('Configure the fields provided for each <em>sense</em> on a dictionary entry.') . '</p>';
  }
}

/**
 * Implements hook_lingwo_entry_properties().
 */
function lingwo_senses_lingwo_entry_properties() {
  return array(
    'senses' => array(
      'class' => 'LingwoSenses',
    ),
  );
}

/**
 * Implements hook_theme().
 */
function lingwo_senses_theme() {
  return array(
    'lingwo_senses_original_form' => array(
      'render element' => 'form',
      'file' => 'lingwo_senses.theme.inc',
    ),
    'lingwo_senses_translation_form' => array(
      'render element' => 'form',
      'file' => 'lingwo_senses.theme.inc',
    ),
    'lingwo_senses_original' => array(
      'variables' => array('node' => NULL, 'options' => NULL),
      'file' => 'lingwo_senses.theme.inc',
    ),
    'lingwo_senses_translation' => array(
      'variables' => array('node' => NULL, 'options' => NULL),
      'file' => 'lingwo_senses.theme.inc',
    ),
    'lingwo_senses_relationship' => array(
      'variables' => array('node' => NULL, 'sense' => NULL),
      'file' => 'lingwo_senses.theme.inc',
    ),
  );
}

/**
 * Implements hook_lingwo_entry_form_alter().
 */
function lingwo_senses_lingwo_entry_form_alter(&$form, &$form_state, &$node) {
  $func = '_lingwo_senses_widget_' . (_lingwo_is_translation($node) ? 'translation' : 'original');
  $form['lingwo_senses'] = $func($form, $form_state, $node);
  $form['lingwo_senses']['#weight'] = 1;

  $form['#attached']['js'][] = drupal_get_path('module', 'lingwo_senses') . '/lingwo_senses.js';
}

/**
 * Implements hook_field_extra_fields().
 */
function lingwo_senses_field_extra_fields() {
  $type = LingwoEntry::$settings->content_type;

  foreach (array('form', 'display') as $which) {
    $extra['node'][$type][$which] = array(
      'lingwo_senses' => array(
        'label' => t('Lingwo Senses'),
        'description' => t('Lingwo dictionary "Senses" field.'),
        'weight' => 1,
      ),
    );
  }

  return $extra;
}

function _lingwo_senses_clean(&$senses) {
  unset($senses['wrapper']);

  // retire senses
  $senses = array_filter($senses, function ($item) {
    if (isset($item['remove']) && $item['remove']) {
      return FALSE;
    }
    return TRUE;
  });

  $weight = array();
  foreach ($senses as &$sense) {
    // weight and some other extra stuff..
    if (isset($sense['weight'])) {
      $weight[] = $sense['weight'];
    }
    unset($sense['weight'], $sense['remove'], $sense['new'], $sense['message']);

    // if 'same_as' is set, then we clear the other fields
    if (!empty($sense['data']['same_as'])) {
      unset($sense['data']['trans']);
      unset($sense['data']['clue']);
    }
    else {
      unset($sense['data']['same_as']);
    }
    // clear translations on no_equivalent
    if (!empty($sense['data']['no_equivalent'])) {
      $sense['data']['trans'] = array();
    }
    else {
      unset($sense['data']['no_equivalent']);
    }
    // remove empty translations
    if (isset($sense['data']['trans'])) {
      $sense['data']['trans'] = array_filter($sense['data']['trans']);
    }

    // if "is" is set, clear difference
    if (!empty($sense['data']['is']['relationship'])) {
      unset($sense['data']['difference']);
    }
    else {
      unset($sense['data']['is']);
    }

    // remove empty fields
    foreach (array('example', 'example_translation', 'clue') as $name) {
      if (empty($sense['data'][$name])) {
        unset($sense['data'][$name]);
      }
    }
  }

  if (!empty($weight) && count($weight) == count($senses)) {
    // put in the order by weight (but only if it was given)
    array_multisort($weight, SORT_ASC, $senses);
  }
}

/**
 * Implements hook_node_load().
 */
function lingwo_senses_node_load($nodes, $types) {
  if (in_array(LingwoEntry::$settings->content_type, $types)) {
    $res = db_query("SELECT nid, delta, data FROM {lingwo_senses_revisions} WHERE vid IN (:vid)",
      array(':vid' => array_map(function ($node) { return $node->vid; }, $nodes)));

    foreach ($res as $row) {
      $nodes[$row->nid]->lingwo_senses[$row->delta] = unserialize($row->data);
    }
  }
}

/**
 * Implements hook_node_insert().
 */
function lingwo_senses_node_insert($node) {
  if (LingwoEntry::isEntryNode($node)) {
    if (!empty($node->lingwo_senses)) {
      _lingwo_senses_clean($node->lingwo_senses);
    }

    foreach ($node->lingwo_senses as $delta => $data) {
      $object = (object) array(
        'nid' => $node->nid,
        'vid' => $node->vid,
        'delta' => $delta,
        'data' => $data,
      );

      drupal_write_record('lingwo_senses_revisions', $object);
    }
  }
}

/**
 * Implements hook_node_update().
 */
function lingwo_senses_node_update($node) {
  if (LingwoEntry::isEntryNode($node)) {
    if (!empty($node->lingwo_senses)) {
      _lingwo_senses_clean($node->lingwo_senses);
    }

    // remove any old senses that where here
    if (empty($node->revision)) {
      db_delete('lingwo_senses_revisions')
        ->condition('vid', $node->vid)
        ->execute();
    }

    foreach ($node->lingwo_senses as $delta => $data) {
      $object = (object) array(
        'nid' => $node->nid,
        'vid' => $node->vid,
        'delta' => $delta,
        'data' => $data,
      );

      drupal_write_record('lingwo_senses_revisions', $object);
    }
  }
}

/**
 * Implements hook_node_view().
 */
function lingwo_senses_node_view($node, $view_mode = 'full') {
  if (LingwoEntry::isEntryNode($node)) {
    $theme = 'lingwo_senses' . (_lingwo_is_translation($node) ? '_translation' : '_original');

    // TODO Please change this theme call to use an associative array for the $variables parameter.
    $node->content['lingwo_senses'] = array(
      '#markup' => theme($theme, array('node' => $node)),
    );
  }
}

/**
 * Implements hook_node_revision_delete().
 */
function lingwo_senses_node_revision_delete($node) {
  if (LingwoEntry::isEntryNode($node)) {
    db_delete('lingwo_senses_revisions')
      ->condition('vid', $node->vid)
      ->execute();
  }
}

/**
 * Implements hook_node_delete().
 */
function lingwo_senses_node_delete($node) {
  if (LingwoEntry::isEntryNode($node)) {
    db_delete('lingwo_senses_revisions')
    ->condition('nid', $node->nid)
    ->execute();
  }
}

/**
 * Implements hook_i18n_string_refresh().
 */
function lingwo_senses_i18n_string_refresh($group) {
  $count = 0;

  if ($group == 'lingwo') {
    foreach (language_list() as $language) {
      foreach (LingwoSenses::getRelationshipOptions($language->language) as $key => $value) {
        i18n_string_update('lingwo:field:relationship:option_' . $key, $value);
        $count++;
      }
    }
  }

  return $count;
}

/**
 * Implements hook_lingwo_language_form().
 */
function lingwo_senses_lingwo_language_form($language) {
  $value = $language->getValue('lingwo_senses_relationship_allowed_values', LingwoSenses::$settings->relationship_allowed_values);
  $form['lingwo_senses_relationship_allowed_values'] = array(
    '#type' => 'textarea',
    '#title' => t('Relationship Allowed Values'),
    '#default_value' => $value,
    '#description' => t('A list of allowed values for the "Relationship" field.  Leave blank to disable relationships all together.  Use the pipe symbol if you want the part of speech to have an alternate display name, for example: <em>pp|Past Participle</em>.'),
  );
  return $form;
}

/**
 * Implements hook_views_api().
 */
function lingwo_senses_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'lingwo_senses'),
  );
}

